# Venv papkani aktiv qilish
# venv\Scripts\activate
#proyekt boshlash uchun quyidagi kod yoziladi
# django-admin startproject lesson1 .      <---  bunda lesson1 papka shu python fayllar joylashgan joyda paydo bo'ladi
# va bu lesson1 papkani ichida bir nechta python fayllar mavjud bo'ladi
#agar lesson1 dan keyin nuqta qo'yilmasa asosiy papkani ichida lesson1 papka chiqadi va uni ichida yana 1ta lesson1 papka
# ochilgan bo'ladi bu degani tepada aytilgan bir nechta python fayllar 1ta lesson1 papkani ichida emas balki ichma ich papkani ichida bo'ladi
# bu papkani ochish uchun quyidagi buyruq beriladi

def example():
    print('Misol uchun yozildi')