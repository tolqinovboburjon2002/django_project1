import datetime


def koordinate(x, y):
    k = ''
    if x > 0 and y > 0:
        return 'Birinchi chorak!!!'
    elif x > 0 and y < 0:
        return 'To\'rtinchi chorak!!!'
    elif x < 0 and y < 0:
        return 'Uchinchi chorak!!!'
    elif x < 0 and y > 0:
        return 'Ikkinchi chorak!!!'

    return 'Kordinata o\'qining ustida joylashgan!!!'


# print(koordinate(-2, 3))


def butunSon(n):
    for i in range(1, n + 1):
        print(str(i) * i)


# butunSon(10)

def butunSon2(n):
    for i in range(1, n + 1):
        for j in range(i):
            print(i, end='')
        print()


# butunSon2(5)

def tekislik():
    import random as rn
    a = [(rn.randrange(100) - 50, rn.randrange(100) - 50) for i in range(20)]
    max_val = max_index = min_index = 0
    min_val = a[0][0] ** 2 + a[0][1] ** 2
    for i, j in a:
        q = i ** 2 + j ** 2
        if q > max_val:
            max_val = q
            max_index = a.index((i, j))
        if q < min_val:
            min_val = q
            min_index = a.index((i, j))

    return a, a[min_index], a[max_index]


# l, min_q, max_q = tekislik()
# print(l)
# print(min_q)
# print(max_q)

tel_comp = {
    '93': 'Ucell',
    '94': 'Ucell',
    '90': 'Beeline',
    '91': 'Beeline',
    '97': 'Uzmobile',
    '99': 'Uzmobile',
    '33': 'Humans',
    '88': 'Humans',
}


def tel_raqam(a):
    import re
    a = re.findall(r'\+998[\s-]?[(]?\d{2}[)]?[\s-]?\d{3}[\s-]?\d{2}[\s-]?\d{2}', a)

    for i in a:
        code = re.findall(r'\+998[\s-]?[(]?(\d{2})[)]?', i)
        if tel_comp.get(code[0]):
            print(i, tel_comp.get(code[0]))
        else:
            print(i, 'Bunday telefon raqam mavjud emas')


# tel_raqam('+998936579742 +998 (88) 963-98-89 +998659896365')

import datetime


class Student:

    def __init__(self, yil, oy, kun):
        self.yil = yil
        self.oy = oy
        self.kun = kun

    def kun_metod(self):
        cur_y = int(datetime.datetime.now().strftime('%Y'))
        cur_m = int(datetime.datetime.now().strftime('%m'))
        cur_d = int(datetime.datetime.now().strftime('%d'))
        return (cur_y - self.yil) * 365 + (cur_m - self.oy) * 30 + (cur_d - self.kun)

    def soat(self):
        return self.kun_metod() * 24


a = Student(1997, 10, 4)
print(a.kun_metod())
print(a.soat())
