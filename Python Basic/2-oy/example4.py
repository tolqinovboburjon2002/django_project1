# # class Hisobla:
# #     def __init__(self, a, b):
# #         self.a = a
# #         self.b = b
# #
# #     def __init__(self, a, b, c):
# #         self.a_emas = a
# #         self.b = b
# #         self.c = c
# #
# #     def yigindi(self):
# #         return self.a_emas + self.b
# #
# #     def kupaytma(self):
# #         return self.a_emas * self.b
# #
# #     def kupYigindi(self, *d):
# #         return sum(d)
# #
# #     def kupKupaytma(self, *d):
# #         p = 1
# #         for i in d:
# #             p *= i
# #         return p
# #
# #
# # a = Hisobla(10, 23, 45)
# # print(a.yigindi())
# # print(a.kupaytma())
# # print(a.kupYigindi(1, 2, 3, 4, 56, 7))
# # print(a.kupKupaytma(1, 2, 3, 4, 56, 7, 323, 435, 4))
#
# # class Person:
# #     def __init__(self, ism):
# #         self.ism = ism
# #
# #     def name(self):
# #         print(f'Name {self.ism}')
# #
# #
# # class Student(Person):
# #     def uqush(self):
# #         print('Student o\'qiydi')
# #
# #
# # class Xodim(Person):
# #     def ishchi(self):
# #         print('Ishchi ishlaydi')
# #
# #
# # s = Student('Student')
# # s.name()
# # s.uqush()
# # x = Xodim('Xodim')
# # x.name()
# # x.ishchi()
#
#
# # class Person:
# #     def __init__(self, ism='Person'):
# #         self.ism = ism
# #
# #     def name(self):
# #         print(f'Bu ota klas. Name {self.ism}')
# #         return self.ism
# #
# #
# # class Student(Person):
# #     def __init__(self):
# #         super().__init__("Student")
# #
# #     def name(self):
# #         # ismi = Person.name(self)
# #         # ismi = super().name()
# #         ismi = super(Student, self).name()
# #         print(f'Student o\'qiydi {ismi}')
# #
# #
# # class Xodim(Person):
# #     def __init__(self):
# #         Person.__init__(self, 'Xodim')
# #
# #
# # s = Student()
# # s.name()
# # x = Xodim()
# # x.name()
#
#
# # class Hayvonlar:
# #     def __init__(self, b):
# #         self.b = b
# #
# #     def xususiyat(self, a):
# #         print(f'{self.b} {a}')
# #
# #
# # class Qushlar(Hayvonlar):
# #     def __init__(self):
# #         super().__init__('Qushlar')
# #
# #
# # class Burgut(Qushlar):
# #     def __init__(self):
# #         Hayvonlar.__init__(self, 'Burgut')
# #
# # q = Qushlar()
# # q.xususiyat('Uchadi')
# # b = Burgut()
# # b.xususiyat('Ko\'zi o\'tkir')
#
# # class Person:
# #     def gapir(self):
# #         print('Gapiradi')
# #
# #
# # class Qushlar:
# #     def uchadi(self):
# #         print('Uchadi')
# #
# #
# # class Toti(Person, Qushlar):
# #     pass
# #
# #
# # t = Toti()
# # t.gapir()
# # t.uchadi()
#
# # class Kupburchak:
# #     def __init__(self, a, b):
# #         self.a = a
# #         self.b = b
# #
# #     def peremetr(self):
# #         return self.a + self.b
# #
# #     def yuza(self):
# #         return self.a * self.b
# #
# #
# # class Uchburchak(Kupburchak):
# #     def __init__(self, a, b, c):
# #         super(Uchburchak, self).__init__(a, b)
# #         self.c = c
# #         # print(self.peremetr() + c)
# #
# #     def peremetr(self):
# #         p = super(Uchburchak, self).peremetr()
# #         return p + self.c
# #
# #     def yuza(self):
# #         y_peremtr = self.peremetr() / 2
# #         return (y_peremtr * (y_peremtr - self.a) * (y_peremtr - self.b) * (y_peremtr - self.c))**0.5
# #
# #
# # class Tortburchak(Kupburchak):
# #     def __init__(self, a, b):
# #         super(Tortburchak, self).__init__(a, b)
# #
# #     def peremetr(self):
# #         return super(Tortburchak, self).peremetr() * 2
# #
# #     def yuza(self):
# #         return Kupburchak.yuza(self)
# #
# # # a = input()
# #
# #
# # u = Uchburchak(3, 4, 5)
# # print(u.yuza())
# # print(u.peremetr())
# #
# # t = Tortburchak(10, 20)
# # print(t.yuza())
# # print(t.peremetr())
#
#
# # class Hayvonlar:
# #     def __init__(self, xususiyat):
# #         self.xususiyati = xususiyat
# #
# #     def xususiyat(self):
# #         return self.xususiyati
# #
# #
# # class UyHayvonlari(Hayvonlar):
# #     def __init__(self):
# #         super(UyHayvonlari, self).__init__("Uy hayvonlari")
# #
# #
# # class Qushlar(Hayvonlar):
# #     def __init__(self):
# #         super().__init__("Qushlar")
# #
# #
# # class Mushuk(UyHayvonlari):
# #     def __init__(self):
# #         Hayvonlar.__init__(self, 'Miyovlaydi')
# #
# #     def xususiyat(self):
# #         return f'{super(Mushuk, self).xususiyat()}. {self.xususiyati}'
# #
# # class Qarga(Qushlar):
# #     def __init__(self):
# #         Hayvonlar.__init__(self, "Qarg'a qag'illaydi")
# #
# #     def xususiyat(self):
# #         return f'{super(Qarga, self).xususiyat()}. {self.xususiyati}'
# #
# #
# # m = Mushuk()
# # print(m.xususiyat())
# # q = Qarga()
# # print(q.xususiyat())
#
# class Hayvonlar:
#     def __init__(self, a):
#         self.a = a
#
#     def yugurish(self):
#         print(self.a, " yerda yuguradi!!!")
#
#
# class Qushlar:
#     def __init__(self, a):
#         self.a = a
#
#     def xususiyat(self):
#         print(self.a, ' tuxum qo\'yadi!!!')
#
#
# class TuyaQush(Hayvonlar, Qushlar):
#     def __init__(self):
#         super(TuyaQush, self).__init__("TuyaQush")
#
#
# t = TuyaQush()
# t.yugurish()
# t.xususiyat()


# Modullar

# import coordinate
# coordinate.son()

# import funksiyalar
# import classlar
#
# funksiyalar.f1()
# c = classlar.Class1()
# c.metod1()


# import funksiyalar as fn
# import classlar as cl
#
# fn.f1()
# fn.f2()
# c = cl.Class1()
# c.metod1()

# from funksiyalar import f1, f2
#
# f1()
# f2()

# import logging as lgg
# import datetime
# #
# # # lgg.basicConfig(filename='myLog.log', level=lgg.DEBUG)
# lgg.basicConfig(filename='myLog.log', level=lgg.ERROR)
# #
# # lgg.info(f'Bu shunchaki xabar {datetime.datetime.now()}')
# lgg.error(f'Xatolik {datetime.datetime.now()}')
# lgg.critical(f'Xatolik {datetime.datetime.now()}')

# import webbrowser
# webbrowser.open('youtube.com')

# import uuid
# print(uuid.uuid4())
# print(uuid.uuid1())

# import pickle
#
# # a = [34, 34, 45, 56, 76, 67]
# # f = open('pickle.pkl', 'wb')
# # pickle.dump(a, f)
#
# f = open('pickle.pkl', 'rb')
# a = pickle.load(f)
# print(a)

# import platform
# print(platform.system())
# print(platform.uname())

# import os
# print(os.getcwd())
# print(os.system('ipconfig'))

# import tkinter
#
# tk = tkinter.Tk()
#
# def f1():
#     print('Birinchi funksiya')
#
#
# b = tkinter.Button(tk, text='Button', command=f1)
#
# b.pack()
# tk.mainloop()

# import socket
# print(socket.gethostbyname('google.com'))
# print(socket.gethostbyname('youtube.com'))
# print(socket.gethostbyname('www.kun.uz'))
