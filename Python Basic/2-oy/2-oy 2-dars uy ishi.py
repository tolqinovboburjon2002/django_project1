# class ClassName:
# tanasi

# class meningklasim:
#     x=3
#
# a=meningklasim()
# b=meningklasim()
# a.x=45
# print(a.x)
# print(b.x)


# class MeningClasim:
#     x=30
#     def meningMetodim(self, a):
#         # print(f'Birinchi metodimiz {self.x}')
#         return f'Birinchimetodimiz!!! {self.x+a}'
#
# a=MeningClasim()
# print(a.meningMetodim(52))

#   __init__   boshlang'ich qiymatlarni maydon sifatida qabul qilib berish

# class MeningKlasim:
#     def __init__(self,a,b):
#         self.a=a
#         self.b=b
#
# def yigindi(self):
#         return (self.a + self.b)
#     def daraja(self):
#         print(self.yigindi()**(1/2))
#
# a=MeningKlasim(61,20)
# a.yigindi()
# a.daraja()


# class Uchburchak:
#     def __init__(self,a,b,c):
#         self.a=a
#         self.b=b
#         self.c=c
#     def shart(self):
#         if (self.a + self.b > self.c) and (self.b + self.c > self.a) and (self.a + self.c > self.b):
#             print("bu qiymatlar mos keladi davom etamiz")
#         else:
#             print("bu qiymatlardan uchburchak yasab bo'lmaydi")
#
#
#     def perimetr(self):
#         return (self.a+self.b+self.c)
#     def perimetrYarim(self):
#         return (self.perimetr()/2)
#     def yuza(self):
#         return (((self.perimetrYarim())*(self.perimetrYarim()-self.a)*(self.perimetrYarim()-self.b)*(self.perimetrYarim()-self.c))**(1/2))
#
#
# a=Uchburchak(a=float(input("a=")), b=float(input("b=")), c=float(input("c=")))
# a.shart()
# a.perimetr()
# a.perimetrYarim()
# a.yuza()
# print(f'Perimetri = {a.perimetr()}, Yarim perimetri = {a.perimetrYarim()}, Yuzasi = {a.yuza()}')
# if a.yuza()==0:
#     print("yuza nol bo'lishi mumkin emas!!!")
#
# elif a.perimetrYarim() == a.yuza():
#     print("Yuzasi va Yarim perimetri teng")
# else:
#     print("uchala kattalik ham turlicha")


# Uyga topshiriq

class Hisobla:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def yigindi(self):
        return self.a + self.b

    def kopaytma(self):
        return self.a * self.b

    def KopYigindi(self, *d):
        return sum(d)

    def KopKopaytma(self, *d):
        p = 1
        for i in d:
            p *= i
        return p


a = Hisobla(10, 23,45)
print(a.yigindi())
print(a.kopaytma())
print(a.KopYigindi(1, 2, 3, 4, 56, 7))
print(a.KopKopaytma(1, 2, 3, 4, 56, 7))
