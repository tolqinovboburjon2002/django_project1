                                                    #FUNKSIYALAR
# c=10
# def mening_funksiyam(*misol):
#     print(misol, type(misol))
#     print(sum(misol))
#     print(4152+misol[1])
#
# mening_funksiyam(1,2,3,244,45676)
# print(c)

# def ikkinchi_funksiya(**c):
#     print(c)
#     print(c.values())
#     print(c.keys())
#     print(c.items())
#
# ikkinchi_funksiya(a=1, b=2, c=2435)

# def hisobla(a,b):
#     print(a+b)
#     return a+b
# a=hisobla(1,2)
#
# print(hisobla(a,a+1))

# def my_func(n):
#     if n<=1:
#         return 1
#     else:
#         # print(n, end=' ')
#         return n+my_func(n-1)
#
# print(my_func(4))


#lambda param1, param2, ......: ifoda

# x=lambda a: a**2
# print(x(2))

for i in range(10):
    for j in range(10):
        print(j, end=' ')
        print()