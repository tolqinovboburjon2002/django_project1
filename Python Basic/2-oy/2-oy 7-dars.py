# RegEx

import re

a = 'Hello Uzbekistan. Congratulation for 30 years of Independens U_Z_B@gmail.com'
#
# #[] -- to'rtburchak qavs ichiga simvollar joylash
# #  . (nuqta) biror bir belgini kutish(yamgi qatordan tashqari)
# s= re.sindall(r'[ani].{3}', a)
# s= re.sindall(r'[ani]{3}', a)
# #[].+   --  bu belgi to'rtburchak qavs ichida berilgan simvoldan keyin kamida 1ta belgi kelishini anglatadi
#
# s=re.findall(r'[n].+', a)
#
# # [].* -- bu belgi to'rtburchak qavs ichidagi simvoldan keyin biror belgi kelishi shart emasligini bildiradi
#
# s=re.findall(r'[n].*',a)

# \ maxsus ketma-ketlik signali

# \A -- Qidirilayotgan belgi berilgan matnning boshidaligini tekshiradi

# s = re.findall(r'\ASaaalom Uz', a)

# \b -- kiritilgan belgi matnning boshida yoki oxirida bo'lsa tekshiradi

# s = re.findall(r'\bSa', a)-- bu manashu belgi bn boshlanadigan belgilarni chiqaradi
# s = re.findall(r'Sa\b', a)-- bu manashu belgi bn tugaydigandigan belgilarni chiqaradi

# \B --bu belgi matnning tarkibida bo'lsa, lekin boshida yoki oxirida bo'lmasa tekshiradi

# s=re.findall(r'\BSa', a)-- shu belgi bn boshlanmasa lekin tarkibda bo'lsa chiqaradi
# s=re.findall(r'Sa\B', a)-- shu belgi bn tugamasa lekin tarkibda bo'lsa chiqaradi

# \d -- 0-9 raqam ishtirok etgan bo'lsa chiqaradi

# s=re.findall(r'\d',a)
# s=re.findall(r'\d{4}',a)

# \D -- 0-9 gacha raqam bo'lmasa chiqaradi

# s=re.findall(r'\D',a)
# s=re.findall(r'\D{4}',a)

# \s -- bo'sh joylarni chiqaradi

# s=re.findall(r'\s',a)

# \S -- bo'sh joylardan tashqar barcha belgilarni chiqaradi

# s=re.findall(r'\S',a)

# \w -- Harflar, raqamlar va tag chiziqlarni chiqaradi

s = re.findall(r'\w', a)

# \W -- Harflar, raqamlar, tag chiziqlar dan tashqari barcha belgini chiqaradi

# s=re.findall(r'\W',a)

# \Z -- Matnning oxiri kiritilgan belgi bn tugasa ekranga chiqaradi

# s=re.findall(r'm\Z',a)

# ^ bu belgi \A belgi bn bir xil ishlaydi
# $ belgi \Z bilan bir xil ishlaydi
# [^a-zA-Z, 0-9]   shu belgilardan tashqari barcha belgilarni chiqaradi
# s=re.findall(r'[^a-zA-Z./0-9]',a)
print(s)


# import re
#
# a = 'Karta raqami 8600 1232 2541 5663 dan iborat 9800 7859 6523 5263'
# s = re.findall(r'(\d{4} \d{4} \d{4} \d{4})\s?', a)
# print(s)


