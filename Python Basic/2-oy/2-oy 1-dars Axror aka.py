# maktab = []
#
# while 1:
#     a = int(input("1. Maktab qo'shish\n2. O'quvchi qo'shish\n3. Maktablarni ko'rish\n4. O'quvchilarni ko'rish\n"))
#
#     if a == 1:
#         m_name = input("Maktab nomini kiriting: ")
#         m_nomeri = input("Maktab nomerini kiriting: ")
#         if m_name != '' and m_nomeri != '':
#             maktab.append({'nomi': m_name, 'nomeri': m_nomeri, 'uquvchi': []})
#     elif a == 2:
#         for i in maktab:
#             print(f'{maktab.index(i) + 1}. {i.get("nomi")}')
#
#         n = int(input("Qasi maktabga o'quvchi qo'shmoqchisiz? "))
#
#         ism = input("O'quvchining ism-familiyasini kiriting: ")
#
#         maktab[n-1].get('uquvchi').append(ism)
#
#     elif a == 3:
#         if maktab:
#             for i in maktab:
#                 print(f'Maktab nomi: {i.get("nomi")}')
#                 print(f'Maktab nomeri: {i.get("nomeri")}')
#                 print(f'Maktab o\'quvchilarining soni: {len(i.get("uquvchi"))}')
#                 print()
#         else:
#             print('Hali maktablar mavjud emas!!!')
#     elif a == 4:
#         for i in maktab:
#             print(f'Maktab nomi: {i.get("nomi")}')
#             print(f'Maktab nomeri: {i.get("nomeri")}')
#
#             if i.get('uquvchi'):
#                 for j in i.get('uquvchi'):
#                     print(f'\t{j}')
#             else:
#                 print("Maktabning o'quvchilari mavjud emas")
#
#             print()


# funksiya sintaksisi
# def funksiya_nomi(param1, param2 ...):
#   tanasi

# c = 10
# m, *n = (1, 2, 3, 244, 45676)
#
#

# O'zgaruvchilarni tuple shaklida qabul qilish
# def mening_funksiyam(a, b, c, *misol):
#     print(misol, type(misol))
#     print(sum(misol))
#     print(4152 + misol[1])
#
#
# mening_funksiyam(1, 2, 3, 3, 244, 45676)
# print(c)


# O'zgaruvchilarni dict qilib qabul qilish
# def ikkinchi_funksiya(**ixtiyoriy):
#     print(ixtiyoriy)
#     print(ixtiyoriy.values())
#     print(ixtiyoriy.keys())
#     print(ixtiyoriy.items())
#     print(ixtiyoriy.get('c'))
#
#
# ikkinchi_funksiya(a=1, b=2, c='fkjsdfjsdkfjsd')

# Default parametr
# def function(a='salom'):
#     print(a)
#
#
# function('Qiymat')

# def hisobla(a, b):
#     # print(a + b)
#     return a, b, 2, 3
#
#
# a = hisobla(1, 2)
# print(a)
# a, b, c, d = (hisobla(a, 1))
#
# print(a, b)

# rekursive

# def my_func(n):
#     if n <= 1:
#         return 1
#     else:
#         print(n, end=' ')
#         return n + my_func(n - 1)
#
#
# print(my_func(10))

# lambda param1, param2 ...: ifoda
# x = lambda a, b: a**b
# print(x(20, 3))

# def my_func(a):
#     return lambda b: lambda c: c + 10 + a * b
#
#
# # x = lambda b: lambda c: c + 10 + a * b
# x = my_func(12)
# c = x(4)
# # c = lambda c: c + 10 + a * b
# # print(c(0))
# print(my_func(28)(2)(4))
