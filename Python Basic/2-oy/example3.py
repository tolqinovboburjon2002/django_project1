# class Hisobla:
#     def __init__(self, a, b):
#         self.a = a
#         self.b = b
#
#     def __init__(self, a, b, c):
#         self.a_emas = a
#         self.b = b
#         self.c = c
#
#     def yigindi(self):
#         return self.a_emas + self.b
#
#     def kupaytma(self):
#         return self.a_emas * self.b
#
#     def kupYigindi(self, *d):
#         return sum(d)
#
#     def kupKupaytma(self, *d):
#         p = 1
#         for i in d:
#             p *= i
#         return p
#
#
# a = Hisobla(10, 23, 45)
# print(a.yigindi())
# print(a.kupaytma())
# print(a.kupYigindi(1, 2, 3, 4, 56, 7))
# print(a.kupKupaytma(1, 2, 3, 4, 56, 7, 323, 435, 4))

# class Person:
#     def __init__(self, ism):
#         self.ism = ism
#
#     def name(self):
#         print(f'Name {self.ism}')
#
#
# class Student(Person):
#     def uqush(self):
#         print('Student o\'qiydi')
#
#
# class Xodim(Person):
#     def ishchi(self):
#         print('Ishchi ishlaydi')
#
#
# s = Student('Student')
# s.name()
# s.uqush()
# x = Xodim('Xodim')
# x.name()
# x.ishchi()


# class Person:
#     def __init__(self, ism='Person'):
#         self.ism = ism
#
#     def name(self):
#         print(f'Bu ota klas. Name {self.ism}')
#         return self.ism
#
#
# class Student(Person):
#     def __init__(self):
#         super().__init__("Student")
#
#     def name(self):
#         # ismi = Person.name(self)
#         # ismi = super().name()
#         ismi = super(Student, self).name()
#         print(f'Student o\'qiydi {ismi}')
#
#
# class Xodim(Person):
#     def __init__(self):
#         Person.__init__(self, 'Xodim')
#
#
# s = Student()
# s.name()
# x = Xodim()
# x.name()


class Hayvonlar:
    def __init__(self, b):
        self.b = b

    def xususiyat(self, a):
        print(f'{self.b} {a}')


class Qushlar(Hayvonlar):
    def __init__(self):
        super().__init__('Qushlar')


class Burgut(Qushlar):
    def __init__(self):
        Hayvonlar.__init__(self, 'Burgut')

q = Qushlar()
q.xususiyat('Uchadi')
b = Burgut()
b.xususiyat('Ko\'zi o\'tkir')

class Person:
    def gapir(self):
        print('Gapiradi')


class Qushlar:
    def uchadi(self):
        print('Uchadi')


class Toti(Person, Qushlar):
    pass


t = Toti()
t.gapir()
t.uchadi()
