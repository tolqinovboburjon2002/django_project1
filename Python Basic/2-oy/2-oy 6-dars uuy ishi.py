import requests
import json

res = requests.get('https://coding2academy.herokuapp.com/students')
print(type(res.text))
d = json.loads(res.text)
ism = [i['first_name'] for i in d]

for i in set(ism):
    print(str(i).ljust(10, ' '), ism.count(i))
