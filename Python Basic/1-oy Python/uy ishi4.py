#Darsdagi topshiriqlar                                                          ##
# #Topshiriq 1                                                                    ##
# a='Assalom dunyo'                                                               ##
# print(len(a))                                                                   ##
#                                                                                 ##
# #Topshiriq 2                                                                    ##
# b=' Hello World! '.strip()                                                      ##
# print(b)                                                                        ##
#                                                                                 ##
# #Topshiriq 3                                                                    ##
# c='Hello World!'.lower()                                                        ##
# d='Hello World!'.upper()                                                        ##
# print(c)                                                                        ##
# print(d)                                                                        ##
#                                                                                 ##
# #Topshiriq 4                                                                    ##
# f='Hello World!'.replace('Hello World!', 'Hi World!')                           ##
# print(f)                                                                        ##
                                                                                ##
##################################################################################

#UYGA VAZIFALAR
#Vazifa 1
# x='Bobur_070_'
# print(x[1:3].upper())
#
# #Vazifa 2
# n=int(input('N='))
# s=input('S=')
# print(s[len(s)-n:].rjust(n, '.'))

#Vazifa 3
# n1=int(input('N1='))
# n2=int(input('N2='))
# s1=(input('S1='))
# s2=(input('S2='))
# print(s1[:n1] + s2[:n2])

#Vazifa 4
# c=input('Belgini kiriting: ')
# s=input('so\'zni kiriting: ')
# a=s.replace(c, c*2)
# print(a)

#Vazifa 5
# s=input("So'zni kiriting:")
# n=s.strip().count(' ')
# print(n+1)

