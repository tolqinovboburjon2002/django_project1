#x, y, z, n
# [i, j, k]
# 0 <= i <= x
# 0 <= j <= y
# 0 <= k <= z
# i + j + k != n

# x = int(input('x='))
# y = int(input('y='))
# z = int(input('z='))
# n = int(input('n='))
#
# a = [[i, j, k] for i in range(x + 1) if i > 0 for j in range(y + 1) for k in range(z + 1) if i + j + k == n]
# print(a)
# # Office77
import random

a = [random.randint(1, 20) for i in range(random.randint(15, 20))]
# a = [2, 3, 7, 7, 7, 6, 6, 5]
print(a)
a.sort()
# a = [2, 3, 5, 6, 6, 7, 7, 7]

c = a[0]
_min = a[0]
# _min = 2
_max = a[0]
# _max = 2

# for i in a:
#     if i > _max:
#         c = _min
#         _min = _max
#         _max = i

# # 1-qadam
# # i = 2 == _max=2
# # 2-qadam
# # i = 3 > _max=2
# # _min = _max = 2
# # _max = i = 3
# # i = 5 > _max = 3
# # _min = _max = 3
# # _max = i = 5
# # i = 6 > _max=5
# # _min = _max = 5
# # _max = i = 6
# # i = 6 == _max = 6
# # i = 7 > _max = 6
# # _min=_max = 6
# # _max = _min = 7
# print(f'Birinchi tezlik {_max} Ikkinchi tezlik {_min} uchinchi {c}')

# tuple
# a = [1, 2, 3, 4, 5, 6]

# print(a, type(a))
# # print(b, type(b))
#
# print(a[2])
# print(a[3:])
# print(a[:4])
# print(a[-2])
# print(a[1:4:2])

# b = ('j', 2, 3, 4, 5, 6)
# x = list(b)
# x.append('salom')
# c = x.pop(2)
# print(x, type(x))
# print(c)
# b = tuple(x)
# print(b, type(b))

# b = ('j', 2, 3, 4, 5, 6)
# # del b
# a = (12, )
# v = tuple((12, ))
# print(v, type(v))
# print(a, type(a))

# b = (4, 5, 6)
# a, v, c = b
#
# print(v)
# print(c)
# print(a)


# b = (1, 2, 4, 6, 'dsfsd', 'sdfsd')
#
# a, *c, z, f = b
# print(a)
# print(c)
# print(z)

# b = (1, 2, 4, 6, 'dsfsd', 'sdfsd')
#
# for i in b:
#     print(i, end=' ')
#     print(b.index(i))
#
# i = 0
# while i < len(b):
#     print(i)
#     i += 1
#
# for i in range(len(b)):
#     print(b[i], end=' ')

# a = (1, 2, 34, 5, 6)
# b = ('a', 'b', 'v', 'sdf')
#
# print(a+b)

# Funksiyalar
# 1. count()
# 2. index()
#
# a = (1, 2, 4, 5, 'sdas', 'dasd', 1)
# print(a.count(0))
# print(len(a))

# import random
#
# lst16 = []
# for i in range(10):
#     lst16.append(random.randrange(30))
# print(lst16)
# n = len(lst16) - 1
# for i in range(len(lst16)//2):
#     print(lst16[i], lst16[n - i], end=',')
#
# print()
# for i in range(len(lst16)):
#     if i % 2 == 0:
#         print(lst16[i], end=' ')
#     else:
#         print(lst16[len(lst16)-i], end=' ')


# import random
#
# a = [random.randint(1, 30) for i in range(10)]
#
# print(a)
# k = 0
#
# # a[1] 2 n-1 n-2 3 4 n-3 n-4
# n=len(a) - 1
# for i in range(0, len(a)//2, 2):
#     print(a[i], a[i+1], a[n-i], a[n-i-1], end=' ')
