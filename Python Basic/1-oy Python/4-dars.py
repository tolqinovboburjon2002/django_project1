# print(complex(1))
# print(float(1))
# print(int(1.2345))
# #Round funksiyasi
# print(round(1.65432, 3))
#x,y,z=input('sonlarni kiriting: ').split()
# x, y, z = map(float, input('sonlarni kirting: ').split())
# print(y,type(y))

#o'zgaruvchining qiymatining uzunlignni chiqarish
 #a='Boburjon To`lqinov'
# print(len(a))
#Yozilgan matnda so'ralgan o'zgaruvchi  bor yo'qligini tekshiruvchi funksiya
# print('s' in 'Assalom')
# print('s' not in 'Assalom')

"""Index har doim noldan boshlanadi. Indekslash"""
#b='Salomefrvedver'
# print(b[4])
# print(b[2:4])
# print(b[:4])# bu birinchi belgidan 4chi belgigacha chiqarradi
# print(b[4:len(b)])#<= bunda 4chi belgidan boshlab oxirigacha oladi
# print(b[4:])#bunda ham 4chi belgidan oxirgi belgigacha chiiqaradi
# ikki nuqta orqali oraliqni belgilashimiz mumkin. Quyidagicha print(b[2:4])
# print(b[-1])
# print(b[-5:-2])
# print(b[-20:])
# print(b[-3:])

#Formatlash
#a="mening yoshim {}. Bo'yim {}"
#a="mening yoshim {1}. Bo'yim {0}"
# a="mening yoshim {a3}. Bo'yim {a2}"
# yosh=input('yoshingizni kirting: ')
# b=input("bo'yingizni kirting:")
# #print(a.format(a3=yosh, a2=b))
#
# print(f'Mening yoshim {yosh}. Bo\'yim {b}')

