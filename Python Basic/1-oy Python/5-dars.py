# Mantiqiy amallar TRUE/False
# print(bool(0))
# print(bool(-1))
# print(bool())


# Arifmetik Operatorlar
# (%)=> Qoldiq
# print(134%5)

# Darajalash
# print(4**(1/2))=> ildiz


# Solishtirish operatorlari doim boolean da keladi
# print(2==3)
# print(2=='2')
# print(2 != 3)
# print(12345>3456)
# print(123<=123)
# print(132>12345)

# Mantiqiy Operatorlar
# AND operatorida berilgan qiymatlarning hammasi rost bo'lsa rost aks holda yolg'on
# print(2>3 and 3<4)

# OR operatori berilgan qiymatlardan kamida bittasi to'g'ri bo'lsa to'g'ri bo'ladi
# print(2>3 or 4>3)

# Not operatori yolg'on qiymatni rost, rost qiymatni yolg'on qilib beradi
# print(not 4>3)

# ANIQLASH OPERATORLARI
# IS operatori Agar ikkala qiymat bir xil obyekt bo’lsa rost qiymat chiqaradi aks holda yolg'on

# Not IS operatori Agar ikkala qiymat bir xil obyekt bo’lmasa True aks holda False

# MEMBERSHIP OPERATORLAR
# IN Operatori Agar berilgan qiymat ketma-ketlik tarkibida bo’lsa True aks holda False
# print('2'in 'swjnjenfjnjqwd2njcdnsjnvknskdf2njnjnjsnjv')

# Not IN operatori Agar berilgan qiymat ketma-ketlik tarkibida bo’lmasa True aks holda False
# print('2efaf'not in 'fwefdas2efahqfgkmergmkmgkemg')


                                          # Binar Operatorlar
# print(2&3)
# print(2|3)
# print(2^3)
# print(~3)
# print(2<<3)
# print(2>>3)

    #Misollar
    #1-topshiriq
# A=float(input("A sonni kiriting: "))
# B=float(input("B sonni kiriting: "))
# C=float(input("C sonni kiriting: "))
# print(A<B<C or A>B>C)

#2-topshiriq
# x=float(input("Sonni kiriting:"))
# y=float(input("Sonni kiriting:"))
# print(x<-1 or y>1 )
#3-topshiriq







#4-topshiriq
# x1=int(input("x1="))
# y1=int(input("y1="))
# x2=int(input("x2="))
# y2=int(input("y2="))
#
# print(x1-x2==y1-y2 or x1+y1==x2+y2)
# a="hello"
# b=(input('b='))
# if a==b:
#     print('')


# a=float(input('Sonni kiriting'))
# if a>0:
#     print('bu son musbat')
# elif a==0:
#     print(' son nolga teng')
# else:
#     print('Manfiy')


