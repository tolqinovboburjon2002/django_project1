# Kiritilgan songacha bo'lgan tub sonlarni chiqaruvchi dastur
# 1-topshiriq
# n=int(input('n='))
# s=0
# for i in range(2,n+1):
#     for j in range(2,i):
#         if i%j==0:
#             break
#     else:
#         s+=1
#         print(f"{s}-tub son {i}")
# print(f'Tub sonlar {s} ta')

# 2-topshiriq Kiritilgan songacha bo'lgan do'st sonlarni chiqaruvchi dastur

n = int(input("n="))
for i in range(1, n):
    s = 0
    for j in range(1, (i // 2) + 1):
        if i % j == 0:
            s += j
    if i < s:
        s1 = 0
        for k in range(1, (s // 2) + 1):
            if s % k == 0:
                s1 += k
        if i==s1:
            print(i,'va',s)