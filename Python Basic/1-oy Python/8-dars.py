# Listlar
# a=[1,2,3,4,5,6,7,8]
# a.remove(1)
# print(a)
# a.remove(4)
# print(a)
# a.remove(1)
# print(a)
# a.pop()
# print(a)
# a.pop(4)
# print(a)

# del a[2] <<== ko'rsatilgan indeksni o'chiradi
# print(a)
# del a     <<== to'liq listni o'chirib tashlaydi
# print(a)

# a.clear()       <<===  to'liq listni tozalaydi
# print(a)


a = [1, 2, 3, 4, 5, 6, 7, 8]
#
# for i in a:
#     print(i)
#
# for i in range(len(a)):
#     print(a[i], end=' ')


i = 0
while i < len(a):
    print(a[i], end=' ')
    i += 1



