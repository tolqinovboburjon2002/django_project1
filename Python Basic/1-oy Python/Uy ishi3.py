#Vazifa1
a=float(input("a tomonning uzunligini kiriting:"))
b=float(input("b tomonning uzunligini kiriting:"))
c=float(input("c tomonning uzunligini kiriting:"))
V=a*b*c
S=2*(a*b+b*c+a*c)
print("Paralellopipedning hajmi: " + str(V) + " metr kub")
print("Paralellopipedning to'la sirti: " + str(S) + " metr kvadrat")

#Vazifa2
r=float(input("Doiraning radiusini kiriting:"))
PI=3.14
L=2*PI*r
S=PI*pow(r,2)
print("Doiraning uzunligi: " + str(L)+ " metr")
print("Doiraning yuzi: " + str(S)+ " metr kvadrat")

#Vazifa3
x=float(input("birinchi sonini kiriting:"))
y=float(input("ikkinchi sonni kiriting:"))
D=(x+y)/2
print("kiritilgan sonlarning o`rta arifmetigi: "+str(D))

#Vazifa4
z=float(input("noldan farqli bo`lgan birinchi sonni kiriting:"))
l=float(input("noldan farqli bo`lgan ikkinchi sonni kiriting:"))
F=z+l
H=z-l
zningkvadrati=z*z
lningkvadrati=l*l
print("sonlarning yig`indisi: "+str(F))
print("sonlarning ayirmasi: "+str(H))
print("birinchi sonning kvadrati: "+str(zningkvadrati))
print("ikkinchi sonning kvadrati: "+str(lningkvadrati))

#Vazifa5
j=float(input("noldan farqli bo`lgan birinchi sonni kiriting:"))
k=float(input("noldan farqli bo`lgan ikkinchi sonni kiriting:"))
E=(j-k)/(j+k)
print("sonlarning ayirmasining yig`indisiga nisbati: "+str(E))


