import sqlite3

conn = sqlite3.connect('DB4.db')
cur = conn.cursor()

while 1:
    b = int(input("""
        1.Termin qo'shish.
        2.Terminlarni saralash.\n      
    """))
    if b == 1:
        termin = input("Termin: ")
        e_translation = input('English: ')
        r_translation = input('Russian: ')

        cur.execute('INSERT INTO termins (uzbek, english, russian) VALUES (?,?,?)',
                    (termin, e_translation, r_translation))
        conn.commit()
        continue
    elif b == 2:
        c = int(input("""
            1.O'sish tartibida saralash
            2.Kamayish tartibida saralash\n
        """))
        if c == 1:
            s = cur.execute('SELECT * FROM termins order by uzbek asc,english desc')
            for i in s:
                print(str(i[0]).ljust(5, '_'), i[1].ljust(20, '_'), i[2].ljust(20, '_'), i[3].ljust(20, '_'))
        # s = cur.execute('SELECT * FROM termins order by uzbek')
        elif c == 2:
            s = cur.execute('SELECT * FROM termins order by uzbek desc,english asc')
            for i in s:
                print(str(i[0]).ljust(5, '_'), i[1].ljust(20, '_'), i[2].ljust(20, '_'), i[3].ljust(20, '_'))
