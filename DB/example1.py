import sqlite3

conn = sqlite3.connect('database.db')
cur = conn.cursor()

# ism = input('Ism ni yozing: ')

# e = cur.execute('SELECT * FROM Student WHERE id="{}"'.format(id)) # inject ga uchrash mumkin holat
# e = cur.execute('SELECT * FROM Student WHERE id>? or email="a@gmail.com"', (id, )) # injection dan saqlanish
# e = cur.execute('SELECT * FROM Student WHERE first_name=? or email="a@gmail.com"', (ism, )) # injection dan saqlanish
# e = cur.execute('SELECT * FROM Student WHERE not (first_name=? and email="a@gmail")', (ism,))  # injection dan saqlanish
# e = cur.execute('SELECT * FROM Student WHERE first_name like "%"')
# e = cur.execute('SELECT * FROM Student WHERE not email like "%com"')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "%b%"')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "e%"')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "e%k" or email="a@gmail" order by first_name desc, last_name asc')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "e%k" or email="a@gmail" limit 1')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "e%k" or email="a@gmail" limit 1 offset 1')
# e = cur.execute('SELECT * FROM Student WHERE first_name like "e%k" or email="a@gmail"')
e = cur.execute('SELECT * FROM Student')
print(e.fetchall()[1:2])
